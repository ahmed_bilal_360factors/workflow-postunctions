package com.workflow.postfunction;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import service.Api;

/**
 * This is the post-function class that gets executed at the end of the transition.
 * Any parameters that were saved in your factory class will be available in the transientVars Map.
 */
@Named("AssignQaTestToCoc")
public class AssignQaTestToCoc extends AbstractJiraFunctionProvider
{
    private static final Logger log = LoggerFactory.getLogger(AssignQaTestToCoc.class);
    public static final String FIELD_MESSAGE = "messageField";
    @ComponentImport
    private final Api api;
    
    @Inject
    public AssignQaTestToCoc(Api api) {
    	this.api=api;
    }
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        MutableIssue issue = getIssue(transientVars);
        String message = (String)transientVars.get(FIELD_MESSAGE);

    	CustomField facilityIdCustomield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Facility Id");
    	CustomField siteIdCustomield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Site Id");
        String url="";
    	Double testSeriesId = (Double) issue.getCustomFieldValue(facilityIdCustomield);
    	Double siteId = (Double) issue.getCustomFieldValue(siteIdCustomield);
        if(testSeriesId!=null && testSeriesId>0) {
			try {
				Map results = api.executeGet("http://localhost:8080/treeapi/qatestseries/getCocByFacilityId/"+testSeriesId.intValue(), Map.class);
				if(results.containsKey("name")) {
		    		String cocName = results.get("name").toString();
		    		String type = results.get("type").toString();
		    		Integer assigneeType = 1;
		    		if(type.equalsIgnoreCase("group")) {
		    			assigneeType = 2;
		    		}else {
		    			cocName = cocName +"@"+ api.getProject().getKey();
		    		}
		    		api.setIssueAssignee(issue, cocName, assigneeType, true, true);
				}	
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
        }else if(siteId!=null) {
			try {
				Map results = api.executeGet("http://localhost:8080/treeapi/qatestseries/getcocbysiteid/"+siteId.intValue(), Map.class);
				if(results.containsKey("name")) {
		    		String cocName = results.get("name").toString();
		    		String type = results.get("type").toString();
		    		Integer assigneeType = 1;
		    		if(type.equalsIgnoreCase("group")) {
		    			assigneeType = 2;
		    		}
		    		api.setIssueAssignee(issue, cocName, assigneeType, true, true);
				}	
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
        }
    }
}