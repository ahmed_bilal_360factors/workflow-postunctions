package com.workflow.postfunction;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.gson.Gson;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import service.Api;

/**
 * This is the post-function class that gets executed at the end of the transition.
 * Any parameters that were saved in your factory class will be available in the transientVars Map.
 */
@Named("AssignToTestingProcessOwner")
public class AssignToTestingProcessOwner extends AbstractJiraFunctionProvider
{
    private static final Logger log = LoggerFactory.getLogger(AssignToTestingProcessOwner.class);
    public static final String FIELD_MESSAGE = "messageField";
    @ComponentImport
    private final Api api;

    @Inject
    public AssignToTestingProcessOwner(Api api) {
    	this.api = api;
    }
    
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        MutableIssue issue = getIssue(transientVars);
        String message = (String)transientVars.get(FIELD_MESSAGE);

        if (null == message) {
            message = "";
        }

    	CustomField testingSeriesId = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Test Series Id");
    	Double testSeriesId = (Double) issue.getCustomFieldValue(testingSeriesId);
        if(testSeriesId!=null) {
			try {
				Map results = api.executeGet("http://localhost:8080/treeapi/qatestseries/"+testSeriesId.intValue(), Map.class);
				if(results.containsKey("testingProcessOwner")) {
		    		String testingProcessOwner = results.get("testingProcessOwner").toString();
		    		testingProcessOwner = testingProcessOwner+ "@" + api.getProject().getKey();
		    		System.out.println(testingProcessOwner);
		    		api.setIssueAssignee(issue, testingProcessOwner, 1, true, true);
				}	
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
        }	
        
        //issue.setDescription(issue.getDescription() + message);
    }
}