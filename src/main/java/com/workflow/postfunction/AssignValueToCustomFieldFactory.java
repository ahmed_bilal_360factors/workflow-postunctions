package com.workflow.postfunction;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.loader.*;
import webwork.action.ActionContext;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This is the factory class responsible for dealing with the UI for the post-function.
 * This is typically where you put default values into the velocity context and where you store user input.
 */

public class AssignValueToCustomFieldFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginFunctionFactory
{
    public static final String FIELD_ID = "customField";
    public static final String FIELD_VALUE = "customFieldValue";

    public AssignValueToCustomFieldFactory() {
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        Map<String, String[]> myParams = ActionContext.getParameters();
//        final JiraWorkflow jiraWorkflow = workflowManager.getWorkflow(myParams.get("workflowName")[0]);

        //the default message
        velocityParams.put(FIELD_ID, "Put id here");
        velocityParams.put(FIELD_VALUE, "Put value here");

    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        if (!(descriptor instanceof FunctionDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor = (FunctionDescriptor)descriptor;
        String customField = (String)functionDescriptor.getArgs().get(FIELD_ID);
        if (customField == null) {
        	customField = "No Message";
        }
        String customFieldValue = (String)functionDescriptor.getArgs().get(FIELD_VALUE);
        if (customFieldValue == null) {
        	customFieldValue = "No Message";
        }        

        velocityParams.put(FIELD_ID,customField);
        velocityParams.put(FIELD_VALUE,customFieldValue);
    }


    public Map<String,?> getDescriptorParams(Map<String, Object> formParams) {
        Map params = new HashMap();

        // Process The map
        String customfield = extractSingleParam(formParams,FIELD_ID);
        String fieldValue = extractSingleParam(formParams,FIELD_VALUE);
        params.put(FIELD_ID,customfield);
        params.put(FIELD_VALUE,fieldValue);

        return params;
    }

}