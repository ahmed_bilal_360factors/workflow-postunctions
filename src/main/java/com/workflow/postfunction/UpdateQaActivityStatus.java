package com.workflow.postfunction;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import service.Api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the post-function class that gets executed at the end of the transition.
 * Any parameters that were saved in your factory class will be available in the transientVars Map.
 */
public class UpdateQaActivityStatus extends AbstractJiraFunctionProvider
{
    private static final Logger log = LoggerFactory.getLogger(UpdateQaActivityStatus.class);
    public static final String FIELD_MESSAGE = "messageField";

    @ComponentImport
    private final Api api;
    
    @Inject
    public UpdateQaActivityStatus(Api api) {
    	this.api=api;
    }    
    
    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        MutableIssue issue = getIssue(transientVars);
        String status = (String)args.get(FIELD_MESSAGE);

        if (null == status) {
            status = "";
        }

        Map<String,String> inputObj = new HashMap<String, String>();
        inputObj.put("jiraKey", issue.getKey());
        inputObj.put("status", status);
        Boolean result;
		try {
			result = api.executePut("http://localhost:8080/treeapi/qatestseries/qatestactivity", inputObj);
			System.out.println("Result is = " + result);
		} catch (Exception e) {
			log.error("Something went wrong with api call", e.fillInStackTrace());
		}
    }
}