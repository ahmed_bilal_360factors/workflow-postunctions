package com.workflow.postfunction;

import java.util.Map;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the post-function class that gets executed at the end of the transition.
 * Any parameters that were saved in your factory class will be available in the transientVars Map.
 */
public class AssignValueToCustomField extends AbstractJiraFunctionProvider
{
    private static final Logger log = LoggerFactory.getLogger(AssignValueToCustomField.class);
    public static final String FIELD_ID = "customField";
    public static final String FIELD_VALUE = "customFieldValue";

    public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        MutableIssue issue = getIssue(transientVars);
        String customField = (String)args.get(FIELD_ID);
        String customFieldValue = (String)args.get(FIELD_VALUE);
        if (null == customField) {
        	customField = "";
        }else {
        	issue.setCustomFieldValue(ComponentAccessor.getCustomFieldManager().getCustomFieldObject(customField), customFieldValue);
        }
        

    }
}