package com.workflow.postfunction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.workflow.function.issue.AbstractJiraFunctionProvider;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.WorkflowException;

/**
 * This is the post-function class that gets executed at the end of the transition.
 * Any parameters that were saved in your factory class will be available in the transientVars Map.
 */
public class CaptureCurrentDateToCustomField extends AbstractJiraFunctionProvider
{
    private static final Logger log = LoggerFactory.getLogger(CaptureCurrentDateToCustomField.class);
    public static final String FIELD_MESSAGE = "messageField";

    @SuppressWarnings({ "deprecation", "rawtypes" })
	public void execute(Map transientVars, Map args, PropertySet ps) throws WorkflowException
    {
        MutableIssue issue = getIssue(transientVars);
        String message = (String)args.get(FIELD_MESSAGE);

        if (null == message) {
            message = "";
        }
        CustomField firstMileStoneDateCF = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName(message);
        Timestamp currentTimeStamp = new Timestamp(new Date().getTime());

        issue.setCustomFieldValue(firstMileStoneDateCF, currentTimeStamp);
//        issue.setDescription(issue.getDescription() + message);
    }
}