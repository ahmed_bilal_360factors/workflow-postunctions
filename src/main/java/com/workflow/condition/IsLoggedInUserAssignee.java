package com.workflow.condition;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.module.propertyset.PropertySet;

import service.Api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import javax.inject.Inject;

public class IsLoggedInUserAssignee extends AbstractJiraCondition
{
    private static final Logger log = LoggerFactory.getLogger(IsLoggedInUserAssignee.class);

    public static final String FIELD_WORD = "word";

    @ComponentImport
    private final Api api;    
    
    @Inject
    public IsLoggedInUserAssignee(Api api) {
    	this.api=api;
    }
    
    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    {
        Issue issue = getIssue(transientVars);
        return api.isLoginUserIssueAssignee(issue);
    }
}
