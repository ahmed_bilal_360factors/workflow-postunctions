package com.workflow.condition;

import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.module.propertyset.PropertySet;

import service.Api;

public class IsAssigneeTestingProcessOwner extends AbstractJiraCondition
{
    private static final Logger log = LoggerFactory.getLogger(IsAssigneeTestingProcessOwner.class);

    public static final String FIELD_WORD = "word";

    @ComponentImport
    private final Api api;
    
    @Inject
    public IsAssigneeTestingProcessOwner(Api api) {
    	this.api=api;
    }
    
    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    {
        Issue issue = getIssue(transientVars);

//     	CustomField facilityIdCustomield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Facility Id");
//    	Double facilityId = (Double) issue.getCustomFieldValue(facilityIdCustomield);
			try {
				return (api.getLoggedInUser().getUsername().equalsIgnoreCase(issue.getReporter().getUsername()));
				//String url="http://localhost:8080/treeapi/qatestseries/gettestprocessownerbyfacility/"+facilityId.intValue();
				//System.out.println("URI = " + url);
//				Map results = api.executeGet(url, Map.class);
//				System.out.println("Result Size = " + results.size());
//				if(results.containsKey("name")) {
//		    		String testProcessOwner = results.get("name").toString();
//		    		String type = results.get("type").toString();
//		    		Integer assigneeType = 1;
//		    		return (issue.getAssignee().getUsername().equalsIgnoreCase(testProcessOwner + "@" + api.getProject().getKey()));
//		    		//return true;
//				}	
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
                
        return false;
    }
}
