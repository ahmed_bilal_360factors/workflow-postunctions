package com.workflow.condition;

import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.Group;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.opensymphony.module.propertyset.PropertySet;

import service.Api;

public class IsAssigneeCOC extends AbstractJiraCondition
{
    private static final Logger log = LoggerFactory.getLogger(IsAssigneeCOC.class);

    public static final String FIELD_WORD = "word";

    @ComponentImport
    private final Api api;
    
    @Inject
    public IsAssigneeCOC(Api api) {
    	this.api=api;
    }
    
    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    {
        String word = (String)transientVars.get(FIELD_WORD);
        Issue issue = getIssue(transientVars);
        String description = issue.getDescription();

     	CustomField facilityIdCustomield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Facility Id");
     	CustomField siteCustomield = ComponentAccessor.getCustomFieldManager().getCustomFieldObjectByName("Site Id");
        String url="";
    	Double facilityId = (Double) issue.getCustomFieldValue(facilityIdCustomield);
    	Double siteId = (Double) issue.getCustomFieldValue(siteCustomield);
        System.out.println("Facility ID >>>>>>>>>" + facilityId);
        System.out.println("Site ID >>>>>>>>>>>>>" + siteId);
    	if(facilityId!=null && facilityId>0) {
			try {
				Map results = api.executeGet("http://localhost:8080/treeapi/qatestseries/getCocByFacilityId/"+facilityId.intValue(), Map.class);
				if(results.containsKey("name")) {
		    		String cocName = results.get("name").toString();
		    		String type = results.get("type").toString();
		    		if(type.equalsIgnoreCase("group")) {
		    			GroupManager groupManager = ComponentAccessor.getGroupManager();
		    			Group group = groupManager.getGroup(api.getProject().getKey()+"-"+cocName);
		    			return groupManager.isUserInGroup(api.getLoggedInUser(), group);
		    		}else {
		    			return (issue.getAssignee().getUsername().equalsIgnoreCase(cocName + "@" + api.getProject().getKey()));
		    		}
				}
			}catch (Exception e) {
				System.out.println(e.getMessage());
				return false;
			}
        }else if (siteId!=null) {
			try {
				System.out.println("SITE CALL");
				System.out.println("http://localhost:8080/treeapi/qatestseries/getcocbysiteid/"+siteId.intValue());
				Map results = api.executeGet("http://localhost:8080/treeapi/qatestseries/getcocbysiteid/"+siteId.intValue(), Map.class);
				if(results.containsKey("name")) {
		    		String cocName = results.get("name").toString();
		    		String type = results.get("type").toString();
		    		Integer assigneeType = 1;
		    		if(type.equalsIgnoreCase("group")) {
		    			GroupManager groupManager = ComponentAccessor.getGroupManager();
		    			Group group = groupManager.getGroup(api.getProject().getKey()+"-"+cocName);
		    			return groupManager.isUserInGroup(api.getLoggedInUser(), group);
		    		}else {
		    			return (issue.getAssignee().getUsername().equalsIgnoreCase(cocName + "@" + api.getProject().getKey()));
		    		}
		    		//return true;
				}	
			}catch (Exception e) {
				System.out.println(e.getMessage());
				return false;
			}
        }
        
        return false;
    }
}
